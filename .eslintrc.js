/* eslint-disable prettier/prettier */
module.exports = {
  parser: '@babel/eslint-parser',
  extends: ['@react-native-community'],
  overrides: [
    {
      files: ['**/*.test.js'],
      rules: {
        'max-lines-per-function': 'off',
        'require-jsdoc': 'off',
      },
    },
  ],
  rules: {
    'require-jsdoc': [
      'error',
      {
        require: {
          FunctionDeclaration: true,
          MethodDefinition: true,
          ClassDeclaration: true,
          ArrowFunctionExpression: true,
          FunctionExpression: true,
        },
      },
    ],
    'valid-jsdoc': 'error',
    'comma-dangle': 'off',
    'curly': 'off',
    'no-underscore-dangle': 'off',
    'global-require': 'off',
    'react-hooks/exhaustive-deps': 'off'
  },
  globals: {
    describe: true,
    expect: true,
    test: true,
    beforeEach: true,
    afterEach: true,
    it: false,
    jest: false,
  },
};
