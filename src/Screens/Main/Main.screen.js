import React from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import {useSelector, useDispatch, Provider} from 'react-redux';
import get from 'lodash/get';

import store from '../../Redux/Store.redux';
import {actions} from '../../Redux/Reducer/Image.reducer';
import {getImageByText, getRecentImage} from '../../Services/API.services';

import SearchInput from '../../Components/SearchInput';
import RecentSearch from '../../Components/RecentSearch';
import Content from '../../Components/Content';
import ImageModal from '../../Components/ImageModal';

import styles from './Main.screen.styles';

const {setImage, setPage, setLoading, setRecentKeywords, setError} = actions;

/**
 * _handleError -> dispatch actions after API request finished with error
 * @param {Function} dispatch - dispatch function
 * @returns {Function} callback function to  call when request failed
 */
const _handleError = dispatch => () => {
  dispatch(setLoading(false));
  dispatch(setError(true));
};

/**
 * _handlePreRequest -> dispatch actions before API request
 * @param {Function} dispatch - dispatch function
 * @returns {void}
 */
const _handlePreRequest = dispatch => {
  dispatch(setError(false));
  dispatch(setLoading(true));
};

/**
 * _onMount -> handle API request on app started,
 * this should call Flicker API to get recent image without search text
 * @param {Function} dispatch - dispatch function
 * @param {number} page - pagination indicator
 * @returns {void}
 */
const _onMount = (dispatch, page) => {
  const params = `&page=${page}`;
  _handlePreRequest(dispatch);

  getRecentImage(
    params,
    response => {
      dispatch(setImage(get(response, 'data.photos.photo', [])));
      dispatch(setPage(1));
      dispatch(setLoading(false));
    },
    _handleError(dispatch),
  );
};

/**
 * _onPageChange -> handle API request when user reach end of scroll,
 * this should call Flicker API to get next page within search text
 * @param {Function} dispatch - dispatch function
 * @param {Array} images - current images from previous request
 * @param {string} currentSearch - current search text
 * @param {number} page - pagination variable
 * @returns {void}
 */
const _onPageChange = (dispatch, images, currentSearch, page) => {
  if (currentSearch === '') return;

  const params = `&text=${currentSearch}&page=${page}`;
  _handlePreRequest(dispatch);

  getImageByText(
    params,
    response => {
      const newImage = [...images, ...get(response, 'data.photos.photo', [])];
      dispatch(setImage(newImage));
      dispatch(setLoading(false));
    },
    _handleError(dispatch),
  );
};

/**
 * _onSearch -> handle API request after user finished typing in search input,
 * this should call Flicker API to get first page within search text
 * @param {Function} dispatch - dispatch function
 * @param {string} currentSearch - current search text
 * @param {number} page - pagination variable
 * @returns {void}
 */
const _onSearch = (dispatch, currentSearch, page) => {
  if (currentSearch === '') return;

  const params = `&text=${currentSearch}&page=${page}`;
  _handlePreRequest(dispatch);

  dispatch(setLoading(true));

  getImageByText(
    params,
    response => {
      dispatch(setImage(get(response, 'data.photos.photo', [])));
      dispatch(setPage(1));
      dispatch(setLoading(false));
      dispatch(setRecentKeywords(currentSearch));
    },
    _handleError(dispatch),
  );
};

/**
 * _renderHeaderSection -> Render header section
 * @returns {React.Node} Header Section
 */
const _renderHeaderSection = () => (
  <View style={styles.headerSection}>
    <Text style={styles.headerTitle}>Image Finder</Text>
    <SearchInput />
    <RecentSearch />
  </View>
);

/**
 * MainScreen -> Main Screen Component
 * This component will handle logic for request and render the main screen
 * @returns {React.Node} Main Screen
 */
export const MainScreen = () => {
  const {currentSearch, images, page} = useSelector(state => state.image);
  const dispatch = useDispatch();

  React.useEffect(() => {
    _onMount(dispatch, page);
  }, []);

  React.useEffect(() => {
    _onSearch(dispatch, currentSearch, page);
  }, [currentSearch]);

  React.useEffect(() => {
    _onPageChange(dispatch, images, currentSearch, page);
  }, [page]);

  return (
    <SafeAreaView style={styles.container}>
      {_renderHeaderSection()}
      <Content />
      <ImageModal />
    </SafeAreaView>
  );
};

export default () => (
  <Provider store={store}>
    <MainScreen />
  </Provider>
);
