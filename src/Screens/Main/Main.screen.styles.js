export default {
  container: {
    backgroundColor: '#309fff',
  },
  headerSection: {
    marginHorizontal: 30,
  },
  headerTitle: {
    fontSize: 30,
    color: '#fff',
    fontWeight: '700',
    marginTop: 20,
  },
};
