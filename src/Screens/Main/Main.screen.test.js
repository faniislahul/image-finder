import React from 'react';
import {render} from '@testing-library/react-native';
import {useSelector, useDispatch} from 'react-redux';

import MainScreen from './Main.screen';

jest.mock('react-redux', () => {
  const {View} = jest.requireActual('react-native');
  const MockProvider = props => <View>{props.children}</View>;
  return {
    useSelector: jest.fn(),
    useDispatch: jest.fn(),
    Provider: MockProvider,
  };
});

jest.useFakeTimers();

describe('Main Screen test', () => {
  const mockState = {
    image: {
      currentSearch: '',
      page: 1,
      images: [
        {
          item: {
            farm: 1,
            server: 'sadeqd32',
            id: '44252',
            secret: 'hhas8syasdiasu',
          },
        },
      ],
    },
  };

  const mockDispatch = jest.fn();
  const mockUseSelector = jest.fn().mockReturnValue(mockState);

  beforeEach(() => {
    useSelector.mockImplementation(() => mockUseSelector);
    useDispatch.mockImplementation(() => mockDispatch);
  });

  it('should return component', () => {
    const component = render(<MainScreen />);

    expect(component).toMatchSnapshot();
  });
});
