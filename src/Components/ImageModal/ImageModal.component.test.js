import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import {useSelector, useDispatch} from 'react-redux';

import ImageModal from './ImageModal.component';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

jest.useFakeTimers();

describe('Image Modal test', () => {
  const mockState = {
    image: {
      selected: 'https://',
    },
  };

  const mockDispatch = jest.fn();
  const mockUseSelector = jest.fn().mockReturnValue(mockState);

  beforeEach(() => {
    useSelector.mockImplementation(() => mockUseSelector);
    useDispatch.mockImplementation(() => mockDispatch);
  });

  it('should return component', () => {
    const component = render(<ImageModal />);

    expect(component).toMatchSnapshot();
  });

  it('should dispact action when input value changed', () => {
    const {getByTestId} = render(<ImageModal />);
    const component = getByTestId('closeButton');
    fireEvent.press(component);

    expect(mockDispatch).toBeCalled();
  });
});
