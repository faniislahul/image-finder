import React from 'react';
import {
  Modal,
  Pressable,
  SafeAreaView,
  Image,
  Text,
  Dimensions,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';

import {actions} from '../../Redux/Reducer/Image.reducer';
import styles from './ImageModal.component.styles';

const width = Dimensions.get('screen').width * 0.9;

/**
 * _onClose -> callback function when close button pressed
 * @param {Function} dispatch - Dispatch function
 * @returns {Function} callback for close button onPress
 */
const _onClose = dispatch => () => {
  dispatch(actions.setSelected(null));
};

/**
 * _onLoad -> callback function when image successfully loaded
 * @param {Function} setHeight - height state setter
 * @returns {Function} callback for image successfully loaded
 */
const _onLoad = setHeight => evt => {
  setHeight(
    (evt.nativeEvent.source.height / evt.nativeEvent.source.width) * width,
  );
};

/**
 * ImageModal -> This component render modal to show individual image
 * @param {Function} setHeight - height state setter
 * @returns {React.Node} Image Modal Component
 */
const ImageModal = () => {
  const dispatch = useDispatch();
  const selected = useSelector(state => state.image.selected);
  const [height, setHeight] = React.useState(400);

  return (
    <Modal visible={selected !== null} animationType="fade" transparent={true}>
      <SafeAreaView style={styles.modalContainer}>
        <Image
          source={{uri: selected}}
          style={styles.image(height, width)}
          onLoad={_onLoad(setHeight)}
        />
        <Pressable
          onPress={_onClose(dispatch)}
          style={styles.closeButton}
          testID="closeButton">
          <Text style={styles.textIcon}>x</Text>
        </Pressable>
      </SafeAreaView>
    </Modal>
  );
};

export default ImageModal;
