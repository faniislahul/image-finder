export default {
  closeButton: {
    height: 30,
    width: 30,
    borderRadius: 15,
    backgroundColor: '#33dd',
    position: 'absolute',
    top: 80,
    right: 30,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#fff',
    borderWidth: 3,
    shadowColor: '#111',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
  modalContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    width: '100%',
    backgroundColor: '#11111177',
  },
  image: (height, width) => ({
    height,
    width,
  }),
  textIcon: {
    color: '#fff',
    fontWeight: '800',
  },
};
