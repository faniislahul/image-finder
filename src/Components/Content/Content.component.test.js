import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import {useSelector, useDispatch} from 'react-redux';

import Content, {_renderItem} from './Content.component';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

jest.useFakeTimers();

describe('Content Component test', () => {
  const mockState = {
    image: {
      currentSearch: '',
      page: 1,
      loading: false,
      images: [
        {
          id: '3342',
        },
      ],
      selected: 'https://',
    },
  };

  const mockDispatch = jest.fn();
  const mockUseSelector = jest.fn().mockReturnValue(mockState);

  beforeEach(() => {
    useSelector.mockImplementation(() => mockUseSelector);
    useDispatch.mockImplementation(() => mockDispatch);
  });

  it('should return component snapshot', () => {
    const component = render(<Content />);

    expect(component).toMatchSnapshot();
  });

  describe('Render Item', () => {
    const mockItem = {
      item: {
        farm: 1,
        server: 'sadeqd32',
        id: '44252',
        secret: 'hhas8syasdiasu',
      },
    };

    it('should return item snapshot', () => {
      const component = render(_renderItem(mockDispatch)(mockItem));

      expect(component).toMatchSnapshot();
    });

    it('should dispact action when image pressed', () => {
      const {getByTestId} = render(_renderItem(mockDispatch)(mockItem));
      const component = getByTestId('contentImage');
      fireEvent.press(component);

      expect(mockDispatch).toBeCalled();
    });
  });
});
