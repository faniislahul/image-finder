export default {
  container: {
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  imageList: {
    height: '80%',
    backgroundColor: '#fff',
  },
  contentImage: imageWidth => ({
    height: 200,
    width: imageWidth,
    marginHorizontal: 5,
    marginBottom: 10,
    borderRadius: 15,
  }),
};
