import React from 'react';
import {View, Text, FlatList, Image, Pressable, Dimensions} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';

import {getURI} from '../../Services/API.services';
import {actions} from '../../Redux/Reducer/Image.reducer';
import styles from './Content.component.styles';

const {setSelected, setPage} = actions;

/**
 * _onImagePress -> callback function when image pressed
 * @param {Function} dispatch - dispatch function
 * @param {string} uri - image uri
 * @returns {Function} callback for when image pressed
 */
const _onImagePress = (dispatch, uri) => () => {
  dispatch(setSelected(uri));
};

/**
 * _renderItem -> render list item
 * @param {Function} dispatch - dispatch function
 * @returns {React.Node} Image List Item
 */
export const _renderItem = dispatch => item => {
  const uri = getURI(item.item);
  const imageWidth = Dimensions.get('window').width / 2 - 30;

  return (
    <Pressable onPress={_onImagePress(dispatch, uri)} testID="contentImage">
      <Image source={{uri}} style={styles.contentImage(imageWidth)} />
    </Pressable>
  );
};

/**
 * _onEndReached -> callback function when list reach end scroll
 * @param {number} page - page state
 * @param {Function} dispatch - dispatch function
 * @returns {Function} callback for when list reach end scroll
 */
export const _onEndReached = (page, dispatch) => () => {
  dispatch(setPage(page + 1));
};

/**
 * _renderLoading -> render loading indicator
 * @returns {React.Node} Loading indicator
 */
const _renderLoading = () => <Text>Loading...</Text>;

/**
 * Content -> ImageFinder content component
 * This component show list of image, handle loading, and handle lazy load.
 * @returns {React.Node} Content component
 */
const Content = () => {
  const dispatch = useDispatch();
  const {loading, images, page, currentSearch} = useSelector(
    state => state.image,
  );

  return (
    <View style={styles.container}>
      {loading && _renderLoading()}
      <FlatList
        data={images}
        renderItem={_renderItem(dispatch)}
        numColumns={2}
        keyExtractor={item => item.id}
        onEndReached={_onEndReached(page, dispatch)}
        style={styles.imageList}
        extraData={`${page}-${currentSearch}`}
        onEndReachedThreshold={0.7}
      />
    </View>
  );
};

export default Content;
