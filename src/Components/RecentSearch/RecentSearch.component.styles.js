export default {
  recentSearchItem: {
    paddingVertical: 5,
    paddingHorizontal: 10,
    backgroundColor: '#11111133',
    borderRadius: 20,
    marginHorizontal: 5,
  },
  recentSearchText: {
    color: '#fff',
  },
  recentSearchContainer: {
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
  },
};
