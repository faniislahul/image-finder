import React from 'react';
import {View, FlatList, Text, Pressable} from 'react-native';
import {useSelector} from 'react-redux';

import styles from './RecentSearch.component.styles';

/**
 * _renderItem -> render recent search item.
 * @param {Object} item - recent search item
 * @returns {React.Node} Item component
 */
const _renderItem = ({item}) => (
  <Pressable style={styles.recentSearchItem}>
    <Text style={styles.recentSearchText}>{item}</Text>
  </Pressable>
);

/**
 * RecentSearch -> recent search component
 * This component show recent search keyword.
 * @returns {React.Node} Item component
 */
const RecentSearch = () => {
  const recentKeywords = useSelector(state => state.image.recentKeywords);

  return (
    <View style={styles.recentSearchContainer}>
      <FlatList
        data={recentKeywords}
        renderItem={_renderItem}
        keyExtractor={item => item}
        horizontal={true}
      />
    </View>
  );
};

export default RecentSearch;
