import React from 'react';
import {render} from '@testing-library/react-native';
import {useSelector} from 'react-redux';

import RecentSearch from './RecentSearch.component';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
}));

describe('Recent Search test', () => {
  const mockState = {
    image: {
      recentKeywords: ['sun', 'flowers', 'summer'],
    },
  };

  beforeEach(() => {
    useSelector.mockImplementation(state => mockState);
  });

  it('should return component', () => {
    const component = render(<RecentSearch />);

    expect(component).toMatchSnapshot();
  });
});
