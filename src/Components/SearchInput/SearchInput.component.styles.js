export default {
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
  },
  textInput: {
    height: 40,
    backgroundColor: '#ffffff22',
    borderRadius: 10,
    color: '#fff',
    fontWeight: '700',
    marginTop: 20,
    flex: 1,
    paddingLeft: 40,
  },
  searchIcon: {
    height: 15,
    width: 15,
    marginRight: -30,
    marginTop: 20,
    tintColor: '#fff',
  },
};
