import React from 'react';
import {TextInput, View, Image} from 'react-native';
import {useDispatch} from 'react-redux';
import debounce from 'lodash/debounce';

import {actions} from '../../Redux/Reducer/Image.reducer';
import styles from './SearchInput.component.styles';
import image from '../../Assets/search.png';

/**
 * setCurrentSearch -> dispatch setSearch action.
 * Function should be debounced to improve perfomance
 * @param {Function} dispatch - dispatch function
 * @param {string} text - search text
 * @returns {void}
 */
const _setCurrentSearch = debounce(
  (dispatch, text) => {
    dispatch(actions.setSearch(text));
  },
  600,
  false,
);

/**
 * setCurrentSearch -> dispatch setSearch action.
 * Function should be debounced to improve perfomance
 * @param {Function} dispatch - dispatch function
 * @param {Function} setValue - value state setter
 * @returns {Function} Callback on search text changed
 */
const _onSearchChange = (dispatch, setValue) => text => {
  setValue(text);
  _setCurrentSearch(dispatch, text);
};

/**
 * setCurrentSearch -> dispatch setSearch action.
 * Function should be debounced to improve perfomance
 * @param {Function} dispatch - dispatch function
 * @param {Function} setValue - value state setter
 * @returns {Function} Callback on search text changed
 */
const SearchInput = () => {
  const [value, setValue] = React.useState('');
  const dispatch = useDispatch();

  return (
    <View style={styles.container}>
      <Image source={image} style={styles.searchIcon} />
      <TextInput
        testID={'SearchInput'}
        style={styles.textInput}
        placeholder="Search image.."
        value={value}
        onChangeText={_onSearchChange(dispatch, setValue)}
      />
    </View>
  );
};

export default SearchInput;
