import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import {useDispatch} from 'react-redux';

import SearchInput from './SearchInput.component';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

jest.useFakeTimers();

describe('Search Input test', () => {
  const mockDispatch = jest.fn();

  beforeEach(() => {
    useDispatch.mockImplementation(() => mockDispatch);
  });

  it('should return component', () => {
    const component = render(<SearchInput />);

    expect(component).toMatchSnapshot();
  });
  it('should dispact action when input value changed', () => {
    const {getByTestId} = render(<SearchInput />);
    const component = getByTestId('SearchInput');
    fireEvent.changeText(component, 'Search');
    jest.runAllTimers();

    expect(mockDispatch).toBeCalled();
  });
});
