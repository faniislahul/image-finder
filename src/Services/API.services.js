import axios from 'axios';

const API_KEY = '11c40ef31e4961acf4f98c8ff4e945d7';
const BASE_ADDRESS = 'https://api.flickr.com/services/rest/';

/**
 * getImageByText -> call Flickr API without search text
 * @param {string} params - request parameter in string
 * @param {Function} onSuccess - callback function to call when request is success
 * @param {Function} onFailed - callback function to call when request is error
 * @returns {void}
 */
export const getRecentImage = (params, onSuccess, onFailed = () => {}) => {
  const url = `${BASE_ADDRESS}?method=flickr.photos.getRecent&api_key=${API_KEY}&format=json&nojsoncallback=1&per_page=20`;
  axios.get(`${url}${params}`).then(onSuccess).catch(onFailed);
};

/**
 * getImageByText -> call Flickr API with search text
 * @param {string} params - request parameter in string
 * @param {Function} onSuccess - callback function to call when request is success
 * @param {Function} onFailed - callback function to call when request is error
 * @returns {void}
 */
export const getImageByText = (params, onSuccess, onFailed = () => {}) => {
  const url = `${BASE_ADDRESS}?method=flickr.photos.search&api_key=${API_KEY}&format=json&nojsoncallback=1&per_page=20`;
  axios.get(`${url}${params}`).then(onSuccess).catch(onFailed);
};

/**
 * getURI -> get image url from photo object
 * @param {Object} item - photo object from Flickr API
 * @returns {string} image url
 */
export const getURI = ({farm, server, id, secret}) => {
  const uri = `https://farm${farm}.static.flickr.com/${server}/${id}_${secret}.jpg`;

  return uri;
};
