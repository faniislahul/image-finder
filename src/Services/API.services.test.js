import axios from 'axios';
import {getRecentImage, getImageByText, getURI} from './API.services';

jest.mock('axios', () => ({
  get: jest.fn().mockImplementation(() => ({
    then: jest.fn().mockImplementation(() => ({
      catch: jest.fn(),
    })),
  })),
}));

const getRecentBaseUrl =
  'https://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=11c40ef31e4961acf4f98c8ff4e945d7&format=json&nojsoncallback=1&per_page=20';

const getSearchBaseUrl =
  'https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=11c40ef31e4961acf4f98c8ff4e945d7&format=json&nojsoncallback=1&per_page=20';

describe('API Services test', () => {
  const mockOnSuccess = jest.fn();
  const mockOnFailed = jest.fn();

  it('should get correct API call when calling getRecentImage', () => {
    const mockParams = '$page=1';
    const expectedResult = getRecentBaseUrl + mockParams;

    getRecentImage(mockParams, mockOnSuccess, mockOnFailed);

    expect(axios.get).toBeCalledWith(expectedResult);
  });

  it('should get correct API call when calling getImageByText', () => {
    const mockParams = '$page=1&text=search';
    const expectedResult = getSearchBaseUrl + mockParams;

    getImageByText(mockParams, mockOnSuccess, mockOnFailed);

    expect(axios.get).toBeCalledWith(expectedResult);
  });

  it('should get correct url when calling getURI', () => {
    const mockParams = {
      farm: 1,
      server: 'sp.1.cloudnet',
      id: 122313,
      secret: 'ad321d23d3eeeg',
    };
    const expectedResult = `https://farm${mockParams.farm}.static.flickr.com/${mockParams.server}/${mockParams.id}_${mockParams.secret}.jpg`;

    const result = getURI(mockParams);

    expect(result).toEqual(expectedResult);
  });
});
