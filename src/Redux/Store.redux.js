import {configureStore} from '@reduxjs/toolkit';

import imageReducer from './Reducer/Image.reducer';

export default configureStore({
  reducer: {
    image: imageReducer,
  },
});
