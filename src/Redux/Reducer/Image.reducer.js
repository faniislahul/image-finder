import {createSlice} from '@reduxjs/toolkit';

// Reducers

/**
 * commonHandler -> common redux handler
 * @param {string} key - state object
 * @returns {Function} reducer handler
 */
const commonHandler =
  key =>
  (state, {payload}) => {
    state[key] = payload;
  };

/**
 * setRecentKeywordHandler -> add recent search keyword
 * @param {Object} state - state object
 * @param {Object} action - action object
 * @returns {void}
 */
const setRecentKeywordHandler = (state, {payload}) => {
  state.recentKeywords = [payload, ...state.recentKeywords];
};

export const ImageSlice = createSlice({
  name: 'image',
  initialState: {
    images: [],
    currentSearch: '',
    page: 1,
    loading: false,
    error: false,
    recentKeywords: [],
    selected: null,
  },
  reducers: {
    setImage: commonHandler('images'),
    setSearch: commonHandler('currentSearch'),
    setError: commonHandler('error'),
    setLoading: commonHandler('loading'),
    setRecentKeywords: setRecentKeywordHandler,
    setPage: commonHandler('page'),
    setSelected: commonHandler('selected'),
  },
});

export const actions = ImageSlice.actions;

export default ImageSlice.reducer;
