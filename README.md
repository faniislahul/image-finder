# Image Finder
This is an image finder app based on Flicker API

## How to run
1. Make sure all of React Native requirements are installed [(Check here)](https://reactnative.dev/docs/environment-setup). Then you can install all the dependencies.
```bash
$ yarn
```

2. Run on your preferred platform

For iOS
```bash
$ yarn ios
```

For android
```bash
$ yarn android
```

## Test
To test the code you can run
```bash
$ yarn test --coverage
```
